// By using higher order fumctions, find the cube of each element in an array
const numbers = [1,2,3,4,5];
function cube(number){
    return number ** 3;
}
const cubes = numbers.map(cube);
console.log(cubes);
//list the elements which are odd

function odd(numb){
    return numb % 2 !== 0;
}
const oddnumber = numbers.filter(odd);
console.log(oddnumber);
//product of the elememts 
function product(a,b){
    return a * b;
}
const productofarray = numbers.reduce(product);
console.log(productofarray);
//list the elements which are more then given length
arr = ["java","html","css","javascript"]
ans = arr.filter((array)=>array.length>=5)
console.log(ans);